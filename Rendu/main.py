import plotly.express as px
from utile import *

#---------------------------------------------
# Récupération des datas via ROOT (Fast API)
#---------------------------------------------

    # Nombre de jours total passés dans chaque pays
# requete_json_1 = get_data('http://127.0.0.1:8000/')
requete_json_1 = [{"pays": "France", "dure": 10}, 
                  {"pays": "Germany", "dure": 20}]

    # Nombre de personne qui ont voyagé dans le pays
# requete_json_2 = get_data('http://127.0.0.1:8000/')
requete_json_2 = [{"pays": "France", "nb_personne": 456},
                  {"pays": "Germany", "nb_personne": 53}]

    # Nombre de personnes qui ont effectué le voyage (lignes)
# requete_json_3 = get_data('http://127.0.0.1:8000/')
requete_json_3 = [{"pays": "France", "nb_personne": 456},
                  {"pays": "Germany", "nb_personne": 53}]

#---------------------------------------------
# Affichage des grapiques
#---------------------------------------------
# pays -> durée
list_pays = []
list_duration = []
for dico in requete_json_1:
    list_pays.append(dico["pays"])
    list_duration.append(dico["dure"])

fig1 = px.choropleth(locations=list_pays, 
                     locationmode="country names", 
                     color=list_duration, 
                     scope="europe")
fig1.show()

# pays -> nb_personne
list_pays = []
list_nb_personn = []
for dico in requete_json_2:
    list_pays.append(dico["pays"])
    list_nb_personn.append(dico["nb_personne"])

fig2 = px.choropleth(locations=list_pays, 
                     locationmode="country names", 
                     color=list_nb_personn, 
                     scope="europe")
fig2.show()


