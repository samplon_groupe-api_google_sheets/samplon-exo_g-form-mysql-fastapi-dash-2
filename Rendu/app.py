from dash import Dash, html, dcc, callback, Output, Input
from utile import get_data
import plotly.express as px
import pandas as pd
import folium

app = Dash(__name__)

def generate_table(dataframe, max_rows=10):
    return html.Table([
        html.Thead(
            html.Tr([html.Th(col) for col in dataframe.columns])
        ),
        html.Tbody([
            html.Tr([
                html.Td(dataframe.iloc[i][col]) for col in dataframe.columns
            ]) for i in range(min(len(dataframe), max_rows))
        ])
    ])

#---------------------------------------------
# Récupération des datas via ROOT (Fast API)
#---------------------------------------------

    # Nombre de jours total passés dans chaque pays
requete_json_1 = get_data('http://127.0.0.1:8000/country/days_visit').json()
df_map_1 = pd.DataFrame(requete_json_1)

    # Nombre de personne qui ont voyagé dans le pays
pays = "France"
requete_pays = get_data(f"http://127.0.0.1:8000/personne_country/{pays}").json()

    # Nombre de personnes qui ont effectué le voyage (lignes)
# requete_json_3 = get_data('http://127.0.0.1:8000/voyages').json()
# print(requete_json_3)

    # Liste des pays les plus visités en fonction des pays d'origine
# requete_json_4 = get_data('http://127.0.0.1:8000/').json()
# print(requete_json_4)

    # Stats: pays le plus visité / moyenne de la durée des visites par pays / 
# personne ayant le plus voyagéjson en pandas
requete_json_5 = get_data('http://127.0.0.1:8000/stats').json()

# Stats 1: 0 pays_plus_visite
df_stats_1 = pd.DataFrame(requete_json_5["pays_plus_visite"], index=[0])
# Stats 2: 1 duree_moyen_par_pays
df_stats_2 = pd.DataFrame(requete_json_5["duree_moyen_par_pays"])
# Stats 3: 2 plus_de_voyage
df_stats_3 = pd.DataFrame(requete_json_5["plus_de_voyage"], index=[0])

#---------------------------------------------
# Affichage des graphiques     
#---------------------------------------------
# pays -> durée
fig1 = px.choropleth(data_frame=df_map_1,
                     locations="pays",
                     locationmode="country names",
                     color="nb_jour_visite",
                     scope="europe")

# pays -> nb_personne
list_pays = []
list_nb_personn = []
list_pays.append(pays)
nb_personne = len(requete_pays["recherche par pays de destination"])
list_nb_personn.append(nb_personne)

fig2 = px.choropleth(locations=list_pays, 
                     locationmode="country names", 
                     color=list_nb_personn, 
                     scope="europe")

#---------------------------------------------
# Layout de l'app DASH:
#---------------------------------------------
title1 = 'Graphique 1: Nombre de jours total passés dans chaque pays'
title2 = 'Graphique 2: Nombre de personne qui ont voyagé dans le pays'
title3 = 'Graphique 3: Nombre de personnes qui ont effectué le voyage (lignes)'
title4 = "Graphique 4: Liste des pays les plus visités en fonction des pays d'origine"
title5 = "Graphique 5:" 
subtitle5_1 = "Stats: pays le plus visité"
subtitle5_2 = "Stats: moyenne de la durée des visites par pays"
subtitle5_3 = "Stats: personne ayant le plus voyagé"

app.layout = html.Div(
    [
    html.H1(children='Title of Dash App', style={'textAlign':'center'}),
    html.H2(children=title1, style={'textAlign':'left'}),
    dcc.Graph(id='choropleth-map',figure=fig1),
    html.H2(children=title2, style={'textAlign':'left'}),
    dcc.Graph(id='choropleth-map',figure=fig2),
    html.H2(children=title3, style={'textAlign':'left'}),
    html.Iframe(id='map', srcDoc=open('samplon-exo_g-form-mysql-fastapi-dash-2/data/map.html').read(), width='95%', height='400'),
    html.H2(children=title5, style={'textAlign':'left'}),
    html.H3(children=subtitle5_1, style={'textAlign':'left'}),
    generate_table(df_stats_1),
    html.H3(children=subtitle5_2, style={'textAlign':'left'}),
    generate_table(df_stats_2),
    html.H3(children=subtitle5_3, style={'textAlign':'left'}),
    generate_table(df_stats_3)
    ],
    style={'padding': '10px 10px 10px 5px',
           'float': 'left',
           'width': '100%',
           'display': 'inline-block'}
)

#---------------------------------------------
# Zone de test du code:
#---------------------------------------------
if __name__ == '__main__':
    app.run(debug=True)