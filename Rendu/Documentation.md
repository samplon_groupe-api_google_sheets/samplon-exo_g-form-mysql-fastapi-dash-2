# Méthodologie:


## Carte 1:
Une carte de l’Europe, couleurs en fonction du nombre de jours total passés dans chaque pays

dico_format = {"pays_residence": "france", 
               "duree": 10}

4. Jours de Visite par Pays
Endpoint : /country/days_visit

[
  {
    "pays": "États-Unis",
    "nb_jour_visite": 12
  }
]


## Carte 2:
Une carte de l'Europe, couleurs en fonction du nombre de personne qui ont voyagé dans le pays.

dico_format = {"pays": "france", "nb_personne": 52}
pays dans le format internationnal

1. Données par Pays
Endpoint : /personne_country/{country}

"recherche par pays de destination": len([liste dico personne])



## Carte 3:
Une carte de l’Europe avec une ligne entre chaque pays d’origine et chaque pays visité. L'épaisseur de la ligne doit dépendre du nombre de personnes qui ont effectué le voyage.


1. Données par Pays
Endpoint : /personne_country/{country}

{
  "recherche par pays de résidence": [
    {
      "prenom": "Prenom1",
      "nom": "Nom1",
      "email": "email1@exemple.com",
      "pays_residence": "Brésil",
      "voyages": [
        {
          "pays": "Allemagne",
          "nb_jour_visite": 2
        }
      ]
    }
  ],
  "recherche par pays de destination": [
    {
      "prenom": "Prenom6",
      "nom": "Nom6",
      "email": "email6@exemple.com",
      "pays_residence": "Canada",
      "voyages": "Brésil",
      "duree": 28
    }
  ]
}



## Carte 4:
Une liste des pays les plus visités (avec nombre de visites) en fonction des pays d'origine des utilisateurs. (on choisit le pays dans un dropdown).




## Carte 5:
Une vue qui affiche les statistiques fournies par l'API.

Endpoint : /stats

{
  "pays_plus_visite": {
    "pays": "Japon",
    "nb_visite": 5
  },
  "duree_moyen_par_pays": [
    {
      "pays": "Canada",
      "duree_moyen": 25.0
    }
  ],
  "plus_de_voyage": {
    "id_personne": 8,
    "prenom": "Prenom8",
    "nom": "Nom8",
    "nombre_de_voyage": 3
  }
}


## Root

1. Données par Pays
Endpoint : /personne_country/{country}

2. Données par Nom de Personne
Endpoint : /personne_name/{name}

3. Statistiques de Voyage
Endpoint : /stats

4. Jours de Visite par Pays
Endpoint : /country/days_visit

5. Voyage Vers
Endpoint : /person/travel_to

6. Voyages
Endpoint : /voyages