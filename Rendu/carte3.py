import requests
import folium
import pandas as pd


def generer_map(df_country, df_voyages):
    def make_lines(x):
        folium.PolyLine([(x['lat_x'], x['lon_x']), (x['lat_y'], x['lon_y'])], 
                        color='blue',
                        weight=int(1 * (1 + x['normal'] * 10))).add_to(map)
    def make_markers(x):
        folium.Marker(
            location=[x['lat'], x['lon']],
            tooltip=x.name,
            popup=x.index,
            icon=folium.Icon(icon="cloud"),
        ).add_to(map)
    map = folium.Map(location=(df_country['lat'].mean(), 
                               df_country['lon'].mean()), 
                               zoom_start=5)
    df_country.apply(make_markers, axis=1)
    df_voyages.apply(make_lines, axis=1)
    map.save("data/map.html")


if __name__ == "__main__":
    df = pd.read_json("https://gitlab.com/onfaittout/onfaittout-mysql/-/raw/main/data/data_pays_ue.json?ref_type=heads")
    df = df.T
    df_voyages =pd.read_json("http://127.0.0.1:8000/voyages")[['pays','pays_residence']]
    df_voyages.values.sort()
    df_voyages["pays"] = df_voyages["pays"].str.lower()
    df_voyages["pays_residence"] = df_voyages["pays_residence"].str.lower()
    df_voyages = df_voyages.groupby(["pays","pays_residence"]).value_counts().to_frame()
    df_voyages['normal'] = (df_voyages - df_voyages.min()) / (df_voyages.max() - df_voyages.min())
    df_voyages.reset_index(inplace=True)
    df_voyages = df_voyages.merge(df, left_on="pays_residence", right_index=True)
    df_voyages = df_voyages.merge(df, left_on="pays", right_index=True )
    generer_map(df, df_voyages)

